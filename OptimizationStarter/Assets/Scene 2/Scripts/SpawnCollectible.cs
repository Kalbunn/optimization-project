﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCollectible : MonoBehaviour
{
    //the collectible
    public GameObject Collectible;

    //the current collectible
    private GameObject m_currentCollectible;

    // Update is called once per frame
    void Update()
    {
        //if no currently collectible
        if ( m_currentCollectible == null )
        {
            //Spawn collectible in a random position from the list of positions
            m_currentCollectible = Instantiate( Collectible, this.transform.GetChild( UnityEngine.Random.Range( 0, this.transform.childCount ) ).position, Collectible.transform.rotation );
        }
    }
}
