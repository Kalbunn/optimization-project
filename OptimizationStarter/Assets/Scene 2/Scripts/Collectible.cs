﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    //public float Radius;

    //public void Update()
    //{
    //    //very overcomplicated ontrigger enter, but super overcomplicated
    //    Collider[] collidingColliders = Physics.OverlapSphere( this.transform.position, Radius );
    //    for ( int colliderIndex = 0; colliderIndex < collidingColliders.Length; ++colliderIndex )
    //    {
    //        if ( collidingColliders[colliderIndex].tag == "Player" )
    //        {
    //            Destroy( this.gameObject );
    //        }
    //    }
    //}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Destroy(this.gameObject);
        }
    }
}
