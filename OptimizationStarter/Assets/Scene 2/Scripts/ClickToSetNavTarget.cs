﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickToSetNavTarget : MonoBehaviour
{
    
	// Update is called once per frame
	void Update ()
    {
        //Camera camera = GameObject.Find( "Main Camera" ).GetComponent<Camera>();
        RaycastHit hit = new RaycastHit();
        //If the mouse is on the screen, AND ALSO IT CLICKS ?? 
        if ( Physics.Raycast( Camera.main.ScreenPointToRay( Input.mousePosition ), out hit, float.MaxValue, LayerMask.GetMask( "Ground" ) ) && Input.GetMouseButtonDown(0) )
        {
            GetComponent<NavMeshAgent>().destination = hit.point;
        }
	}
}
