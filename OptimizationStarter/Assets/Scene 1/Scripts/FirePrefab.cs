﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( Camera ) )]
public class FirePrefab : MonoBehaviour
{
    public GameObject Prefab;
    public float FireSpeed = 5;
    public float fireRate = 0.06f;

    // Update is called once per frame
    //void Update()
    //{
    //    if ( Input.GetButton( "Fire1" ) )
    //    {
    //        Vector3 clickPoint = GetComponent<Camera>().ScreenToWorldPoint( Input.mousePosition + Vector3.forward );
    //        Vector3 FireDirection = clickPoint - this.transform.position;
    //        FireDirection.Normalize();
    //        GameObject prefabInstance = GameObject.Instantiate( Prefab, this.transform.position, Quaternion.identity, null );
    //        prefabInstance.GetComponent<Rigidbody>().velocity = FireDirection * FireSpeed;
    //    }
    //}

    private void Start()
    {
        StartCoroutine(FiringSquad());
    }
    
    private IEnumerator FiringSquad()
    {
        //if the button is fired
        if (Input.GetButton("Fire1"))
        {
            //get the camera position
            Vector3 clickPoint = GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition + Vector3.forward);
            //get the firing direction
            Vector3 FireDirection = clickPoint - this.transform.position;
            //normalize that shit
            FireDirection.Normalize();
            //
            GameObject prefabInstance = GameObject.Instantiate(Prefab, this.transform.position, Quaternion.identity, null);
            prefabInstance.GetComponent<Rigidbody>().velocity = FireDirection * FireSpeed;
        }
        yield return new WaitForSeconds(fireRate);
        StartCoroutine(FiringSquad());
    }

    //why is fredds name and email not in the contact info on the course website, this is discrimination
    //its ok fredd, we think ur cool, even if the course website doesnt want to believe u exist
}
