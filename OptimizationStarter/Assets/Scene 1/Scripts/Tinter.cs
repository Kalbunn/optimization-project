﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tinter : MonoBehaviour
{
    public Material[] m_randoMat;
    // Use this for initialization
    void Start()
    {
        // Picks a random colour from the entire array of colours and applies it to the material
        //GetComponentInChildren<Renderer>().material.color = new Color( UnityEngine.Random.Range( 0f, 1f ), UnityEngine.Random.Range( 0f, 1f ), UnityEngine.Random.Range( 0f, 1f ) );

        //Selects a random colour from an array of preset materials
        GetComponentInChildren<Renderer>().material.color = m_randoMat[Random.Range(0, m_randoMat.Length)].color;

        this.enabled = false;
    }

}
